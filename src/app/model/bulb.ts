export class Bulb {
    status :boolean;
    onColor : string;
	offColor: string;
	
	constructor (status : boolean, onColor : string,offColor: string){
		this.status = status;
		this.onColor = onColor;
		this.offColor = offColor;
	}
}