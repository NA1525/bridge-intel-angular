import {Bulb} from './bulb';
import {ClockIdentifier} from './clockIdentifier';


export class ClockConfiguration{
	
	clockIdentifier: ClockIdentifier;
	clockConfig : Map<string , Array<Bulb>>;
	
	constructor(clockIdentifier: ClockIdentifier,clockConfig : Map<string , Array<Bulb>>){
		this.clockConfig  = clockConfig;
		this.clockIdentifier = clockIdentifier;
	}
}