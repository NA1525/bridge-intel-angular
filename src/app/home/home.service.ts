import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private baseUrl = 'http://localhost:8080/clock-ws/v1';

  constructor(private http: HttpClient) { }

  getClock(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/getClock/${id}`);
  }
}