import { Component, OnInit } from '@angular/core';
import {Bulb} from '../model/bulb';
import { ClockIdentifier} from '../model/clockIdentifier';
import { ClockConfiguration } from '../model/clockConfiguration';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	
  clockConfiguration :ClockConfiguration;
  clockIdentifier : ClockIdentifier;
  bulb : Bulb ;
  service : HomeService;

  constructor(bulb : Bulb,clockIdentifier : ClockIdentifier
  			,clockConfiguration :ClockConfiguration, service : HomeService) {
	this.bulb = bulb;
	this.clockIdentifier = clockIdentifier;
	this.clockConfiguration = clockConfiguration;
	this.service= service;
 }

  ngOnInit(): void {
	
  }

  showClock(){
    /*this.bulb.status = true;
    this.bulb.onColor = "#FFFF66";
    this.bulb.offColor = "#FF3333";*/
    this.service.getClock(12121);
 
 }

}
